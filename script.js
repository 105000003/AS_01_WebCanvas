var canvas;
var context;

window.onload = function() {
  canvas = document.getElementById("drawingCanvas");
  context = canvas.getContext("2d");
  canvas.onmousedown = startDrawing;
  canvas.onmouseup = stopDrawing;
  canvas.onmouseout = stopDrawing;
}

var isDrawing = false;
var start_x;
var start_y;
var end_x;
var end_y;
var drawcircle = false;
var flag = false;
var drawline = false;
var drawsquare = false;
var drawtriangle = false;
var fill = false;
var eraser = false;
var eraserSize = number;
var inputtext = false;
var addimage = false;

/*enterText*/
function inputText(){
    //window.alert("eraser");
    document.getElementById("bod").style.cursor = "url('text.PNG'), auto";
    inputtext = true;
    flag = true;
    eraser = false;
    drawtriangle = false;
    drawline = false;
    drawcircle = false;
    drawsquare = false;
    addimage = false;
}

/*eraser*/
function Eraser(){
    //window.alert("eraser");
    document.getElementById("bod").style.cursor = "url('eraser.PNG'), auto";
    eraser = true;
    inputtext = false;
    drawtriangle = false;
    flag = false;
    drawline = false;
    drawcircle = false;
    drawsquare = false;
    addimage = false;
    var e = document.getElementById("eraser").value;
    eraserSize = e;
    drawLine_do();
}

/*Draw a circle*/
function drawCircle(){
    document.getElementById("bod").style.cursor = "url('circle.PNG'), auto";
    drawtriangle = false;
    flag = true;
    inputtext = false;
    drawline = false;
    drawcircle = true;
    eraser = false;
    drawsquare = false;
    addimage = false;
}

/*Draw a Line*/
function drawLine() {
    document.getElementById("bod").style.cursor = "url('line.PNG'), auto";
    drawtriangle = false;
    drawline = true;
    inputtext = false;
    drawsquare = false;
    drawcircle = false;
    flag = false;
    eraser = false;
    addimage = false;
    drawLine_do();
}

/*Draw a square*/
function drawSquare() {
    document.getElementById("bod").style.cursor = "url('square.PNG'), auto";
    drawtriangle = false;
    drawline = false;
    inputtext = false;
    flag = true;
    eraser = false;
    drawcircle = false;
    drawsquare = true;
    addimage = false;
}

/*Draw a triangle*/
function drawTringle(){
    document.getElementById("bod").style.cursor = "url('triangle.PNG'), auto";
    drawtriangle = true;
    drawline = false;
    inputtext = false;
    drawsquare = false;
    drawcircle = false;
    eraser = false;
    addimage = false;
    flag = true;
}

function addImage(){
    document.getElementById("bod").style.cursor = "default";
    drawtriangle = false;
    drawline = false;
    inputtext = false;
    drawsquare = false;
    drawcircle = false;
    eraser = false;
    addimage = true;
    flag = true;
}

/*fill color*/
function fillColor(){
    fill = true;
}

/*not fill color*/
function notFill(){
    fill = false;
}

/* 开始画图*/
function startDrawing(e) {
    //window.alert("eraser");
    //draw circle,square, get x,y
    if((drawcircle == true || drawsquare == true || drawtriangle == true || inputtext == true) && flag == true){
        //window.alert("eraser");
        start_x = e.pageX - canvas.offsetLeft;
        start_y = e.pageY - canvas.offsetTop;
        flag = false;
    }
  isDrawing = true;
  context.beginPath();
  context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

/* 停止画图*/
function stopDrawing(e) {
    //input image
    if(addimage == true && isDrawing == true){
        var img = new Image(1,1); 
        //---------------------------------------------------------------------
        var url_str = document.getElementById("url_string").value;
        img.src = url_str;
        //---------------------------------------------------------------------
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.drawImage(img,end_x,end_y);
        flag = true;
        //----------------------------------------------------------------------
        /*var input = document.getElementById("imageLoader").value;
        var curFiles = input.files;
        img.src = window.URL.createObjectURL(curFiles);
        //---------------------------------------------------------------------
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.drawImage(img,end_x,end_y);
        flag = true;*/
    }
    //input text
    if(inputtext == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        var t_text = document.getElementById("text_text").value;
        var t_size = document.getElementById("text_size").value;
        var t_style = document.querySelector('input[name="text_style"]:checked').value;
        //-------------------------------------
        var fontArgs = context.font.split(' ');
        var newSize = t_size + 'px';
        context.font = newSize + ' ' + t_style;
        //--------------------------------------
        context.fillText(t_text,(start_x+end_x)/2,(start_y+end_y)/2);
        flag = true;
    }
    //draw circle
    if(drawcircle == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.beginPath();
        context.arc(start_x,start_y,(Math.sqrt((start_x-end_x)*(start_x-end_x)+(start_y-end_y)*(start_y-end_y))),0,2*Math.PI);
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    //draw square
    if(drawsquare == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.beginPath();
        context.rect(start_x,start_y,Math.abs(end_x-start_x),Math.abs(end_y-start_y));
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    //draw a triangle
    if(drawtriangle == true && isDrawing == true){
        end_x = e.pageX - canvas.offsetLeft;
        end_y = e.pageY - canvas.offsetTop;
        context.moveTo(start_x,start_y);
        context.lineTo(end_x,end_y);
        context.lineTo(end_x-2*(end_x-start_x),end_y);
        context.lineTo(start_x,start_y);
        context.lineTo(end_x,end_y);
        if(fill == true){
            context.fill();
        }
        context.stroke();
        flag = true;
    }
    isDrawing = false;
}

function drawLine_do(e) {
    if(drawline == true || eraser == true){
        //window.alert("drawLine");
        canvas.onmousemove = drawLine_do;
    }
    //eraser
    if (eraser == true && isDrawing == true){
        var x = e.pageX - canvas.offsetLeft;
        var y = e.pageY - canvas.offsetTop;
        //window.alert("!!");
        context.clearRect(x, y,eraserSize,eraserSize);
    }
    //line
    if (isDrawing == true && drawline == true) {
        var x = e.pageX - canvas.offsetLeft;
        var y = e.pageY - canvas.offsetTop;
        context.lineTo(x, y);
        context.stroke();  
    }
}

function changeColor(colorValue){
    context.strokeStyle = colorValue;
    context.fillStyle = colorValue;
}

/*thickness*/
function changeThickness(thickness){
    if(thickness == -1){
        var x = document.getElementById("thickness").value;
        context.lineWidth = x;
    }
    else{
        context.lineWidth = thickness;
    }
}

/*line*/
function lineStyle(style){
    if(style == 1){
        context.lineCap="round";
    }
    if(style == 2){
        context.lineCap='butt';
    }
}
/* clear*/
function clearCanvas() {
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function saveCanvas() {
    var imageCopy = document.getElementById("savedImageCopy");
    imageCopy.src = canvas.toDataURL(); 
    var imageContainer = document.getElementById("savedCopyContainer");
    imageContainer.style.display = "block";  
}
